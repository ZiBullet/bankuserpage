export function headerCreator (user) {
    // document.body.innerHTML = ''
    let header = document.createElement('header')
    let headerContainer = document.createElement('div')
    let headerPages = document.createElement('div')
    let mainPage = document.createElement('a')
    let myWallets = document.createElement('a')
    let myTransactions = document.createElement('a')

    let headerActions = document.createElement('div')
    let headerUserEmail = document.createElement('span')
    let headerLogOutIcon = document.createElement('img')
    // Decoraing header
    headerContainer.classList.add('header__container')
    headerContainer.classList.add('container')
    headerPages.classList.add('header__container-pages')
    mainPage.href = '../index.html'
    mainPage.innerHTML = 'Главная'
    myWallets.href = './wallets.html'
    myWallets.innerHTML = 'Мои кошельки'
    myTransactions.href = './transactions.html'
    myTransactions.innerHTML = 'Мои транзакции'
    headerActions.classList.add('header__container-actions')
    headerUserEmail.innerHTML = 'dsfsdfdsf'
    headerLogOutIcon.src = '../assets/svg/log-out.svg'
    // Appending elements to header
    headerContainer.append(headerPages, headerActions)
    headerPages.append(mainPage, myWallets, myTransactions)
    headerActions.append(headerUserEmail, headerLogOutIcon)
    header.append(headerContainer)
    document.body.append(header)
}