// ____________________Settings area___________________________
let userInformation = JSON.parse(localStorage.getItem('user'))
if (userInformation === null) {
    window.location.href = './pages/register.html'
}
// _________________________________________________________
let cardContainer = document.querySelector('.content-container')
function reloadCards(cards, place) {
    place.innerHTML = ''
    for (let card of cards) {
        let cardBox = document.createElement('div')
        let cardType = document.createElement('h3')
        let moneyCurrency = document.createElement('span')
        
        cardBox.classList.add('card')
        cardType.innerHTML = card.type
        moneyCurrency.innerHTML = card.currency
        
        cardBox.append(cardType, moneyCurrency)
        place.append(cardBox)
    }
}
reloadCards(userInformation.wallets.slice(0, 4), cardContainer)
function findInfoOfUser(user) {
    for (let info of user) {
        let mainPageTitle = document.querySelector('.my__name')
        let email = document.querySelector('.heading-mail')
        let headerMail = document.querySelector('.header-userEmail')
        headerMail.innerHTML = info.email
        email.innerHTML = info.email
        mainPageTitle.innerHTML = info.name
    }
}
findInfoOfUser([userInformation])
let tbody = document.querySelector('tbody')
console.log(tbody);
function createTransactionList(expenses, place) {
    place.innerHTML = ''
    for (let expense of expenses) {
        let row = document.createElement('tr')
        let id = document.createElement('td')
        let wallet = document.createElement('td')
        let category = document.createElement('td')
        let amount = document.createElement('td')
        let time = document.createElement('td')
        let itemId = expenses.indexOf(expense) + 1
        id.innerHTML = itemId
        wallet.innerHTML = expense.type
        category.innerHTML = expense.category
        amount.innerHTML = expense.amount
        time.innerHTML = expense.time
        row.append(id, wallet, category, amount, time)  
        place.append(row)
    }
}
createTransactionList(userInformation.transactions.slice(0,7), tbody)
let logOut = document.querySelector('.logOut')
logOut.onclick = () => {
    window.location.href = './pages/signin.html'
}